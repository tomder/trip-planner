# Trip Planner

## Installation
```
$ git clone git@bitbucket.org:tomder/trip-planner.git 
$ cd trip-planner
$ composer install 
```

## Basic usage 
```
use App\TripPlanner;

$tp = new TripPlanner(
    10, 
    '4h00', 
    ['Lille', 'Arras', 'Compiègne', 'Paris']
);

$tp->findTrips();
```
Then run:
```
$ ./trip-planner
```

This class takes 3 parameters :

 - **Budget** (*float*), the user's maximum budget
 - **Duration** (*string*|*int*), the user's maximum duration trip
       - *string* with format : XXhYY, or
       - *integer*
 - **Stops** (*array*), the list of every the stops on the bus line

## Usage with CLI parameters 
```
$ ./trip-planner-cli --budget=10 --duration=180 --stops=Lille,Arras,Compiègne,Paris
```

This command takes 3 parameters :

 - **Budget** (*float*), the user's maximum budget
 - **Duration** (*string*|*int*), the user's maximum duration trip
       - *string* with format : XXhYY
       - *integer*
 - **Stops** (*string*), comma separeted list of every the stops on the bus line

## Script 

**ci**```$ composer run-script ci```

 - **phpcs**: ``phpcs ./App --standard=PSR2``
 - **test**: ``phpunit ./tests/BusTest.php --bootstrap ./<br />bootstrap.php``
