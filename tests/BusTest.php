<?php

namespace Tests;

use App\TripPlanner;
use PHPUnit\Framework\TestCase;

class TripPlannerTest extends TestCase
{
    const STOPS_SHORT = ['Lille', 'Arras', 'Compiègne', 'Paris'];
    const STOPS = ['Lille', 'Lesquin', 'Arras', 'Compiègne', 'Paris'];
    const STOPS_LONG = ['Lille', 'Lesquin', 'Hénin-Beaumont', 'Douai', 'Arras', 'Compiègne', 'Paris'];
    
    public function testfindTrips_1Discount_2Flexibility()
    {
        $nbDiscount = 1;
        $nbDuration = 2;

        $tp = new TripPlanner(
            TripPlanner::BASE_PRICE - TripPlanner::DISCOUNT_PER_STOP * $nbDiscount,
            TripPlanner::BASE_TRIP_DURATION + TripPlanner::DURATION_STOP * $nbDuration
        );
        $tp->setStops(self::STOPS_SHORT);
        ob_start();
        $nbResults = $tp->findTrips();
        $nbResultsExpected = $tp->calculateCombinations();
        $nbStopsNeeded = $tp->getStopsNeeded();

        $buffer = ob_get_clean();
        $text = <<<EXPECTED
Lille, Arras, Paris
Lille, Compiègne, Paris
Lille, Arras, Compiègne, Paris
EXPECTED . PHP_EOL;

        $this->assertSame($text, $buffer);
        $this->assertSame($nbResultsExpected, $nbResults);
        $this->assertSame(2, $nbStopsNeeded);
    }

    public function testfindTrips_noDiscount_noFlexibility()
    {
        $tp = new TripPlanner(10, 180);
        $tp->setStops(self::STOPS);
        ob_start();
        $nbResults = $tp->findTrips();
        $nbResultsExpected = $tp->calculateCombinations();
        $nbStopsNeeded = $tp->getStopsNeeded();

        $buffer = ob_get_clean();
        $text = <<<EXPECTED
Lille, Paris
EXPECTED . PHP_EOL;

        $this->assertSame($text, $buffer);
        $this->assertSame($nbResultsExpected, $nbResults);
        $this->assertSame(0, $nbStopsNeeded);
    }

    public function testfindTrips_noDiscount_flexibility()
    {
        $tp = new TripPlanner(10, 360);
        $tp->setStops(self::STOPS);
        ob_start();
        $nbResults = $tp->findTrips();
        $nbResultsExpected = $tp->calculateCombinations();
        $nbStopsNeeded = $tp->getStopsNeeded();

        $buffer = ob_get_clean();
        $text = <<<EXPECTED
Lille, Paris
Lille, Lesquin, Paris
Lille, Arras, Paris
Lille, Compiègne, Paris
Lille, Lesquin, Arras, Paris
Lille, Lesquin, Compiègne, Paris
Lille, Arras, Compiègne, Paris
Lille, Lesquin, Arras, Compiègne, Paris
EXPECTED . PHP_EOL;

        $this->assertSame($text, $buffer);
        $this->assertSame($nbResultsExpected, $nbResults);
        $this->assertSame(3, $nbStopsNeeded);
    }

    public function teststopsCalculation()
    {
        $nbDiscount = 4;
        $nbDuration = 10;
        
        $tp = new TripPlanner(
            TripPlanner::BASE_PRICE - TripPlanner::DISCOUNT_PER_STOP * $nbDiscount,
            TripPlanner::BASE_TRIP_DURATION + TripPlanner::DURATION_STOP * $nbDuration
        );

        $tp->setStops(self::STOPS_LONG);

        $nbStopsRequiredByBudget = $tp->getStopsRequiredByBudget();
        $nbStopsAllowedByDuration = $tp->getStopsAllowedByDuration();
        $nbStopsNeeded = $tp->getStopsNeeded();

        $this->assertSame($nbDiscount, $nbStopsRequiredByBudget);
        $this->assertSame($nbDuration, $nbStopsAllowedByDuration);
        $this->assertSame(5, $nbStopsNeeded);
    }
}
