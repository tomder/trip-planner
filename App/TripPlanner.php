<?php

/**
 * Bonjour !
 *
 * Cette classe Bus est ma réponse à votre exercice
 *
 * @author Thomas D. <deramburethomas@gmail.com>
 * PHP version 7.3.9
 */

namespace App;

use App\Tools;
use Katzgrau\KLogger\Logger;

use InvalidArgumentException;

class TripPlanner
{
    const BASE_PRICE = 10;
    const DISCOUNT_PER_STOP = 2;
    const BASE_TRIP_DURATION = 180;
    const DURATION_STOP = 30;
    const DEFAULT_STOPS = ['Lille', 'Arras', 'Compiègne', 'Paris'];

    /**
     * Array of stops on the bus line
     * @var array
     */
    private $stops = null;

    /**
     * Departure station name
     * @var string
     */
    private $departure = null;

    /**
     * Arrival station name
     * @var string
     */
    private $arrival = null;

    /**
     * Number of stops required by $budget for this trip
     * @var int
     */
    private $stopsRequiredByBudget = null;

    /**
     * Number of stops allowed by $duration for this trip
     * @var int
     */
    private $stopsAllowedByDuration = null;

    /**
     * Number of stops required for this trip
     * @var int
     */
    private $stopsNeeded = null;

    /**
     * Trip budget
     * @var int
     */
    private $budget;

    /**
     * Trip duration time in minutes
     * @var int
     */
    private $duration;

    /**
     * Counter for results
     * @var int
     */
    private $counter;

    /**
     * Logger
     * @var Katzgrau\KLogger\Logger
     */
    private $logger;

    /**
     * Constructor
     *
     * @param float $budget Optional. User's budget constaint for his trip.
     * @param float|string $duration Optional. User's maximum duration constraint for his trip.
     * Expressed in minutes as an integer or as a string using this format XXhYY
     * @param array $stops Optional. List of all stops in the bus line
     * @throws InvalidArgumentException if type of $stop is not array or if length of $stop is inferior to 2
     */
    public function __construct(
        float $budget = self::BASE_PRICE,
        $duration = self::BASE_TRIP_DURATION,
        $stops = self::DEFAULT_STOPS
    ) {
        $stops = $stops ?? self::DEFAULT_STOPS;
        $this->logger = new Logger(__DIR__.'/../logs');

        $this->budget = $budget;
        $this->duration = $this->validateDuration($duration);
        $this->setStops($stops);
        
        $text = 'Init budget: %s | duration: %s | stops: %s';
        $this->logger->debug(sprintf($text, $budget, $duration, implode(', ', $stops)));
    }

    /**
     * Find all trip combinations
     *
     * @return int Number of results
     */
    public function findTrips()
    {
        $this->counter = 0;
        $stops = $this->getStops();
        
        $text = '---> stopsNeeded: %s | stopsAllowedByDuration: %s | stopsRequiredByBudget: %s';
        $this->logger->debug(sprintf(
            $text,
            $this->stopsNeeded,
            $this->stopsAllowedByDuration,
            $this->stopsRequiredByBudget
        ));

        for ($x=$this->stopsRequiredByBudget; ($x <= $this->stopsAllowedByDuration); $x++) {
            $this->recursiveSearch($stops, $x, null);
        }

        $text = 'Total combinations trouvées: %s';
        $this->logger->debug(sprintf($text, $this->counter));

        return $this->counter;
    }

    /**
     * Search available trips recursively
     *
     * @param array     $allStops
     * @param int       $stopsNeeded
     * @param array     $current Optional
     * @param string    $currentKey Optional
     * @param array     $currentKeyArray Optional
     * @param int       $level Optional
     */
    public function recursiveSearch(
        array $stops,
        int $stopsNeeded,
        array $current = null,
        string $currentKey = null,
        array $currentKeyArray = null,
        int $level = 0
    ) {
        $level++;

        /* Populate $current on first iteration */
        if (! $current) {
            $current = $stops;
            $currentKeyArray = [];
        } else {
            if (($key = array_search($currentKey, $current)) !== false) {
                unset($current[$key]);
            }
        }

        if ($currentKey) {
            $currentKeyArray[] = $currentKey;
        }

        if ($level > $stopsNeeded) {
            array_unshift($currentKeyArray, $this->departure);
            array_push($currentKeyArray, $this->arrival);

            $this->counter++;
            $this->printTrip(implode(', ', $currentKeyArray));
            return null;
        }

        foreach ($current as $currentKey) {
            if (count($current) + $level > $stopsNeeded) {
                $this->recursiveSearch($stops, $stopsNeeded, $current, $currentKey, $currentKeyArray, $level);
            }

            /* Remove current step as it can't be used anymore in this loop */
            array_shift($current);
        }
    }

    /**
     * Calculate the expected number of results
     *
     * @return float Result
     */
    public function calculateCombinations()
    {
        $total = 0;
        for ($elements=$this->stopsRequiredByBudget; ($elements <= $this->stopsAllowedByDuration); $elements++) {
            $parmis = count($this->getStops());
            $diff = $parmis - $elements;

            $count = Tools::factorial($parmis) / (Tools::factorial($elements)*Tools::factorial($diff));
            $total += $count;
            
            $text = '%s elements, parmis %s | Diff: %s | Count: %s';
            $this->logger->debug(sprintf($text, $elements, $parmis, $diff, $count));
        }

        $this->logger->debug(sprintf('Total check: %s', $total));
        
        return (int) $total;
    }

    /**
     * Validate $duration parameter
     *
     * @param int|string Either expressed in minutes or matching the following pattern XXhYY
     * @return int if parsing completes successfully, otherwise @throws InvalidArgumentException
     * @throws InvalidArgumentException If an error occurs while parsing $duration
     */
    private function validateDuration($duration)
    {
        if (preg_match_all('/^([0-9])*h([0-9]{0,2})$/i', $duration, $matches)) {
            return ((int) $matches[1][0]) * 60 + (int) $matches[2][0];
        } elseif (preg_match_all('/^\d*$/i', $duration, $matches)) {
            return (int) $duration;
        } else {
            $errorMessage = '$duration: format is not supported';
            $this->logger->error($errorMessage);
            throw new InvalidArgumentException($errorMessage);
        }
    }

    /**
     * Calculate the number of stops needed base on budget and duration constraints
     *
     * @return void
     */
    private function calculateStops(): void
    {
        $this->stopsRequiredByBudget = (int) ceil((self::BASE_PRICE - $this->budget) / self::DISCOUNT_PER_STOP);
        if ($this->stopsRequiredByBudget < 0) {
            $this->stopsRequiredByBudget = 0;
        }

        $this->stopsAllowedByDuration = (int) floor(($this->duration - self::BASE_TRIP_DURATION) / self::DURATION_STOP);
    
        if ($this->stopsAllowedByDuration - $this->stopsRequiredByBudget > 0) {
            $this->stopsNeeded = $this->stopsAllowedByDuration;
        } else {
            $this->stopsNeeded = $this->stopsRequiredByBudget;
        }

        if ($this->stopsNeeded > count($this->stops)) {
            $this->stopsNeeded = count($this->stops);
        }

        $text = 'Budget: %s (%s stops) | Durée %s (%s stops) | Possibles: %s Disponibles: %s';
        $this->logger->debug(sprintf(
            $text,
            $this->budget,
            $this->stopsRequiredByBudget,
            $this->duration,
            $this->stopsAllowedByDuration,
            $this->stopsNeeded,
            count($this->getStops())
        ));
    }

    /**
     * Echos individual trip result
     *
     * @return void
     */
    private function printTrip($trip): void
    {
        echo $trip . PHP_EOL;
    }

    /**
     * Get $stops array
     *
     * @return array
     */
    public function getStops()
    {
        return $this->stops;
    }

    /**
     * Set every stops in the bus line, including departure and arrival ones
     *
     * @param array $stops Array of every stops in this bus line. Including both departure and arrival
     * @return void
     * @throws InvalidArgumentException if type of $stop is not array or if length of $stop is inferior to 2
     */
    public function setStops($stops)
    {
        $this->validateStops($stops);

        $this->departure = array_shift($stops);
        $this->arrival = array_pop($stops);

        $this->stops = $stops;

        $this->calculateStops();
    }

    /**
     * Validate $stops array
     *
     * @throws InvalidArgumentException if type of $stop is not array or if length of $stop is inferior to 2
     */
    private function validateStops($stops)
    {
        if (gettype($stops) !== 'array') {
            $errorMessage = '$stops: array should be an array';
            $this->logger->error($errorMessage);
            throw new InvalidArgumentException($errorMessage);
        }

        if (count($stops) < 2) {
            $errorMessage = '$stops: array length should be equal or superior to 2';
            $this->logger->error($errorMessage);
            throw new InvalidArgumentException($errorMessage);
        }
    }

    /**
     * Returns the calculated number of stops needed
     *
     * @return int
     */
    public function getStopsNeeded()
    {
        return $this->stopsNeeded;
    }

    /**
     * Returns the calculated number of stops required by budget constraint
     *
     * @return int
     */
    public function getStopsRequiredByBudget()
    {
        return $this->stopsRequiredByBudget;
    }

    /**
     * Returns the calculated number of stops allowed by duration constraint
     *
     * @return int
     */
    public function getStopsAllowedByDuration()
    {
        return $this->stopsAllowedByDuration;
    }
}
