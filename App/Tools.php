<?php

namespace App;

class Tools
{
    /**
     * Calculate factorials
     *
     * @param float $number
     * @return float to prevent int overflow
     */
    public static function factorial(float $number): float
    {
        if ($number <= 1) {
            return 1;
        } else {
            return $number * self::factorial($number - 1);
        }
    }
}
